cc = gcc

lex.yy.x: scanner
	$(cc) -o scanner lex.yy.c -lfl

scanner: scanner.l
	flex scanner.l

clean: 
	rm scanner lex.yy.c