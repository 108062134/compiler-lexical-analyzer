%{
    #include <stdio.h>
    #include <string.h>
    int count_line = 0;
    int soruce_on = 1;
    int token_on = 1;
    char *key;
%}

%x  COMMENT
%x  SOURCE
%x  TOKEN

digit  [0-9]
char ('[a-zA-Z]')|('\\t')|('\0')
integer [+-]?[0-9]+
float [+-]?([0-9]+"."[0-9]*)|([0-9]*"."[0-9]+)
identifier [A-Za-z_]+[A-Za-z0-9_]*
tabs [\t]+
spaces [" "]+
newlines [\n]+
oper [-+*/%|=&!]
punctuation [:;,.\]\[}{)(]
others ["#$'><?@\\^_`~]
string \".*\"


%%
[\t ]*"#pragma"[\t ]*source[\t ]*on[\t ]*\n {
    count_line++;
    soruce_on = 1;
    printf("%d:%s",count_line,yytext);
}
[\t ]*"#pragma"[\t ]*source[\t ]*off[\t ]*\n {
    count_line++;
    soruce_on = 0;
}
[\t ]*"#pragma"[\t ]*token[\t ]*on[\t ]*\n {
    count_line++;
    token_on = 1;
    if(soruce_on) printf("%d:%s",count_line,yytext);
}
[\t ]*"#pragma"[\t ]*token[\t ]*off[\t ]*\n {
    count_line++;
    token_on = 0;
    if(soruce_on) printf("%d:%s",count_line,yytext);
}

<SOURCE>[\t ]*"/*".*"*/" {  // 單行註且用/* */

}

<SOURCE>[\t ]*"/*"[^\"*/\"\n]*\n { // 多行註解，進入COMMENT
    BEGIN COMMENT;
    if(soruce_on) printf("%d:%s",count_line,key);
}

<COMMENT>.*"*/"[ \t]*\n|"*/"[ \t]*\n {
    BEGIN 0;
    count_line++;
    if(soruce_on) printf("%d:%s",count_line,yytext);
}

<COMMENT>.*\n { 
    count_line++;
    if(soruce_on) printf("%d:%s",count_line,yytext);
}


[\t ]*\n {  // 純空行不用進SOURCE
    count_line++;
    if(soruce_on) printf("%d:%s",count_line,yytext);
}
.*\n   {    // 一般狀況下每次讀整行/記錄下來 然後開始SOURCE
    count_line ++;
    key = strdup(yytext);
    yyless(0);
    BEGIN SOURCE;
}

<SOURCE>[\n] {  // 遇到換行符號離開SOURCE
    if(soruce_on) printf("%d:%s",count_line,key);
    BEGIN 0 ;
}

<SOURCE>[\t ]*"//".* {

}

<SOURCE>" " {

}

<SOURCE>char|int|float|double|void  {   
    if(token_on) printf("#key:%s\n",yytext);
}
<SOURCE>const|signed|unsigned|short|long    {
    if(token_on) printf("#key:%s\n",yytext);
}
<SOURCE>for|do|while|break|continue|if|else|return|struct|switch|case|default   {
    if(token_on) printf("#key:%s\n",yytext);
}
<SOURCE>NULL|_COUNTER_|_LINE_|INT_MAX|INT_MIN|CHAR_MAX|CHAR_MIN|MAX|MIN {
    if(token_on) printf("#macro:%s\n",yytext);
}

<SOURCE>{string}   {
    if(token_on) printf("#string:%s\n",yytext);
}

<SOURCE>{identifier} {
    if(token_on) printf("#id:%s\n",yytext);
}
<SOURCE>{float} {
    if(token_on) printf("#float:%s\n",yytext);
}
<SOURCE>{integer}   {
    if(token_on) printf("#integer:%s\n",yytext);
}

<SOURCE>{oper}  {
    if(token_on) printf("#op:%s\n",yytext);
}

<SOURCE>{punctuation}   {
    if(token_on) printf("#punc:%s\n",yytext);
}

<SOURCE>{char}  {
    if(token_on) printf("#char:%s\n",yytext);
}


<SOURCE>&&|\|\||!=|>=|<=|==|--|\+\+ {
    if(token_on) printf("#op:%s\n",yytext);
}

%%


int main(void){
    yylex();
    return 0; 
}